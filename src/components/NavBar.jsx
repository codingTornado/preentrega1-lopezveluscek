import CartWidget from "./CartWidget";

export default function NavBar() {
  return (
    <>
      <nav>
        <div>
          <ul>
            <li>
              <h4>i comers</h4>
            </li>

            <li>
              <a href="#">Comics</a>
            </li>

            <li>
              <a href="#">Manga</a>
            </li>

            <li>
              <a href="#">Juegos de mesa</a>
            </li>

          </ul>
        </div>
        <CartWidget />
      </nav>
    </>
  )
}
