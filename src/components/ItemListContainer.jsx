export default function ItemListContainer({ children }) {
  return (
    <div className="card-list">{children}</div>
  )
}
